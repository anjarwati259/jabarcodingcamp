<?php 
require_once('Animal.php');
require_once('Frog.php');
require_once('Ape.php');

$sheep = new Animal("shaun");

echo "name: ". $sheep->name; // "shaun"
echo "<br>";
echo "legs: ". $sheep->legs; // 4
echo "<br>";
echo "Cold Blooded: ". $sheep->cold_blooded; // "no"
echo "<br>";

$kodok = new Frog("buduk");
echo "<br>";
echo "name: ". $kodok->name; // "shaun"
echo "<br>";
echo "legs: ". $kodok->legs; // 4
echo "<br>";
echo "Cold Blooded: ". $kodok->cold_blooded; // "no"
echo "<br>";
echo "Jump: ". $kodok->jump();
echo "<br>";

$sungokong  = new Ape("kera sakti");
echo "<br>";
echo "name: ". $sungokong ->name; // "shaun"
echo "<br>";
echo "legs: ". $sungokong ->legs; // 4
echo "<br>";
echo "Cold Blooded: ". $sungokong ->cold_blooded; // "no"
echo "<br>";
echo "Yell: ". $sungokong ->yell();
echo "<br>";
 ?>
