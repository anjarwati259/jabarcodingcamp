<?php 
require_once('Animal.php');

class Ape extends Animal{
	public $yel = 'Auooo';
	public $legs = 2;

	public function yell(){
		return $this->yel;
	}
 }
 ?>